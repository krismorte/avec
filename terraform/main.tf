
data "aws_subnet_ids" "public_subnet" {
  vpc_id = var.vpc_id
}

 module "fargate_cluster" {
  source = "./modules/fargate_cluster"

}

module "loadbalancer" {
  source = "./modules/loadbalancer_tagetgroup"

  vpc_id = var.vpc_id
}

resource "local_file" "env_file" {
    content     = "cluster_dev=${module.fargate_cluster.fargate_name_dev}\ncluster_prod=${module.fargate_cluster.fargate_name_prod}\necr_app=${module.fargate_cluster.repo_name_app}\ntarget_dev=${module.loadbalancer.lb_target_dev_arn}\ntarget_prod=${module.loadbalancer.lb_target_prod_arn}\nlb_app_sg=${module.loadbalancer.sg_id}"
    filename = "${path.module}/.env"
}