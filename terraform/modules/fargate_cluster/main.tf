resource "aws_ecs_cluster" "cluster_dev" {
  name = "cluster-dev"
  capacity_providers =["FARGATE"]
}

resource "aws_ecs_cluster" "cluster_prod" {
  name = "cluster-prod"
  capacity_providers =["FARGATE"]
}

resource "aws_ecr_repository" "imagem_app" {
  name                 = "app"
}

resource "aws_iam_role" "task_role" {
  name = "ecsTaskExecutionRole"

  assume_role_policy = file("${path.module}/s3-role.json")

}
resource "aws_iam_role_policy_attachment" "task_role_attach" {
  role       = aws_iam_role.task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role_policy_attachment" "task_role_attach2" {
  role       = aws_iam_role.task_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}



output "fargate_name_dev" {
  value = aws_ecs_cluster.cluster_dev.name
}

output "fargate_name_prod" {
  value = aws_ecs_cluster.cluster_prod.name
}

output "repo_name_app" {
  value = aws_ecr_repository.imagem_app.repository_url
}
