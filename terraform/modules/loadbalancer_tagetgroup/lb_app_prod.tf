
resource "aws_lb" "lb_app_prod" {
  name               = "lb-app-prod"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb_sg.id}"]
  subnets            = local.subnet_ids_list

  enable_deletion_protection = false

}



resource "aws_lb_listener" "lb_listener_prod" {
  load_balancer_arn = aws_lb.lb_app_prod.arn
  port              = "80"
  protocol          = "HTTP"

 default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_prod.arn
  }

}

resource "aws_lb_target_group" "lb_target_prod" {
  name        = "lb-target-prod"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id


    health_check {
        enabled             = true
        healthy_threshold   = 5
        interval            = 30
        matcher             = "200"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 5
        unhealthy_threshold = 2
    }

}


output "lb_target_prod_arn" {
  value = aws_lb_target_group.lb_target_prod.arn
}
