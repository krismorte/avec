
data "aws_subnet_ids" "public_subnet" {
  vpc_id = var.vpc_id
}

locals {
  subnet_ids_string = join(",", data.aws_subnet_ids.public_subnet.ids)
  subnet_ids_list = split(",", local.subnet_ids_string)
}
