terraform {
  backend "s3" {
    bucket = "terraform-krisnamourt2.com.br"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}