const express = require('express')
const morgan = require('morgan')
const weather = require('weather-js')
const memoize = require('memoizee')

const { PORT = 8000 } = process.env
const getWeatherCached = memoize(getWeather, { promise: true, maxAge: 30 * 60 * 1000 })

express()
.use(morgan('combined'))
  .set('view engine', 'pug')
  .set('views',__dirname)  
  .get('/', async (req, res) => {
   // res.send('teste')
   res.render('index',{title: 'ola'})
    /*try {
      console.log('aqui2')
      res.render('index',{title: 'ola'});
      res.sendStatus(200);
    } catch (err) {
      res.sendStatus(500)
    }*/
  })
  .get('/search', async (req, res) => {
    const { q: search = 'São Paulo, SP' } = req.query

    try {      
      if(req.query.q){
        console.log('aqui')
        const response = await getWeatherCached(search)     
        res.render('index', {local: response.observationpoint, ceu:response.skytext , temp: 'Temperatura: '+response.temperature+'°'
        , image: response.imageUrl, humidade: 'Umidade: '+response.humidity+'°', vento: 'Vento: '+response.windspeed
        , sensacao:'Sensação: '+response.feelslike+'°'});
      }else{
        res.redirect('/');
      }
      
   
    } catch (err) {
      res.sendStatus(500)
    }
  })
  .listen(PORT, () => {
    console.info(`server started on port ${PORT}`)
  })


async function getWeather (search) {
  return new Promise((resolve, reject) => {
    weather.find({ search, degreeType: 'C' }, (err, result) => {
      if (err) {
        reject(err)
      } else if (!result || !result.length) {
        resolve(null)
      } else {
        resolve(result[0].current)
      }
    });
  })
}
